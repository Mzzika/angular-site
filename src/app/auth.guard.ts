import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import { ApiService } from './services/api.service';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private api:ApiService, private router:Router){}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

      // const isLoggedIn = this.api.isLoggedIn()
      //
      // if(!isLoggedIn) {
      //   // Do an extra check to /login_status to be sure
      //   this.api.loginStatus().subscribe(
      //     () => {
      //       return true
      //     },
      //     error => {
      //       this.router.navigate(["/admin/login"]);
      //       return false
      //     }
      //   )
      // }

      return true
    }
}
