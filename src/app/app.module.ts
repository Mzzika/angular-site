import { BrowserModule, Title } from '@angular/platform-browser';
import { NgModule, ErrorHandler, APP_INITIALIZER } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { HttpModule } from '@angular/http';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
//App routing
import { AppRoutingModule } from './app-routing.module';
//Services
import { ApiService } from './services/api.service';
import { DataService } from './services/data.service';
import { TranslationService } from './services/translation.service';

//Authentication checker
import { AuthGuard } from './auth.guard';
//Environments
import { environment } from 'environments/environment';
//Credentials
import { versionNo } from './credentials/credentials';
//Socket io
import { SocketIoModule, SocketIoConfig } from 'ng-socket-io';
//Modal from ngx-bootstrap
import { ModalModule } from 'ngx-bootstrap/modal';

import { AppComponent } from './app.component';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { LoginPageComponent } from './components/login-page/login-page.component';
import { SignupComponent } from './components/signup/signup.component';
import { ChangePasswordComponent } from './components/change-password/change-password.component';
import { ForgotPasswordComponent } from './components/forgot-password/forgot-password.component'
import { SuperAdminHomeComponent } from './components/super-admin-home/super-admin-home.component';
import { SalesAdminHomeComponent } from './components/sales-admin-home/sales-admin-home.component';
import { ClientAdminHomeComponent } from './components/client-admin-home/client-admin-home.component';
import { CreateUserComponent } from './components/create-user/create-user.component';
import { CreateClientComponent } from './components/create-client/create-client.component';
import { InviteUsersComponent } from './components/invite-users/invite-users.component';
import { RoleComponent } from './components/role/role.component';
import { AcceptInviteComponent } from './components/accept-invite/accept-invite.component';
import { Page404Component } from './components/page-404/page-404.component';
import { Page500Component } from './components/page-500/page-500.component';
import { DomainsComponent } from './components/domains/domains.component';
import { HeaderComponent } from './components/header/header.component';

//Adding Raven to support Sentry
import * as Raven from 'raven-js';
import { ConnectorsComponent } from './components/connectors/connectors.component';

//MomentJS for date formating
import { MomentModule } from 'ngx-moment';
import { CompanyConnectorsSettingsComponent } from './components/company-connectors-settings/company-connectors-settings.component';
import { SearchComponent } from './components/search/search.component';
import { UpdateClientComponent } from './components/update-client/update-client.component';


//Socket.io config
const socketUrl = environment.serverUrl + 'ws';
const config: SocketIoConfig = { url: socketUrl, options: {} };


//MatomoInjector
import { MatomoModule } from 'ngx-matomo';

import { UpdateUserComponent } from './components/update-user/update-user.component';
import { UpdateSubscriberComponent } from './components/update-subscriber/update-subscriber.component';

// Pipes
import { ReplaceUnderscorePipe } from './pipes/replace-underscore.pipe';
import { SortAlphabeticallyPipe } from './pipes/sort-alphabetically.pipe';
import { TranslatePipe } from './pipes/translate.pipe';

// HttpInterceptor
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { ApiInterceptor } from './api.interceptor';
import { AddDomainComponent } from './components/add-domain/add-domain.component';
import { UpdateDomainComponent } from './components/update-domain/update-domain.component';



if (environment.name == 'alpha' || environment.name == 'beta' || environment.name == 'production') {

  Raven
    .config(environment.sentryUrl, {
      environment: environment.name,
      release: versionNo
    })
    .install();

}

export class RavenErrorHandler implements ErrorHandler {
  handleError(err: any): void {
    Raven.captureException(err);
  }
}

export function provideErrorHandler() {
  return new RavenErrorHandler();
}


// Set default i18n language once app is loaded.
export function setupTranslateFactory(service: TranslationService): Function {
  return () => service.setLanguage('en');
}

//

@NgModule({
  declarations: [
    AppComponent,
    LoginPageComponent,
    SignupComponent,
    ChangePasswordComponent,
    ForgotPasswordComponent,
    SuperAdminHomeComponent,
    SalesAdminHomeComponent,
    ClientAdminHomeComponent,
    CreateUserComponent,
    CreateClientComponent,
    InviteUsersComponent,
    RoleComponent,
    AcceptInviteComponent,
    ConnectorsComponent,
    CompanyConnectorsSettingsComponent,
    SearchComponent,
    UpdateClientComponent,
    ReplaceUnderscorePipe,
    UpdateUserComponent,
    UpdateSubscriberComponent,
    Page404Component,
    Page500Component,
    SortAlphabeticallyPipe,
    TranslatePipe,
    DomainsComponent,
    HeaderComponent,
    AddDomainComponent,
    UpdateDomainComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    NgSelectModule,
    HttpModule,
    AppRoutingModule,
    HttpClientModule,
    MomentModule,
    MatomoModule,
    ModalModule.forRoot(),
    SocketIoModule.forRoot(config),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (setupTranslateFactory),
        deps: [HttpClient]
      }
    })
  ],
  providers: [
    ApiService,
    DataService,
    AuthGuard,
    Title,
    TranslationService,
    { provide: APP_INITIALIZER, useFactory: setupTranslateFactory, deps: [ TranslationService ], multi: true },
    { provide: ErrorHandler, useClass: RavenErrorHandler },
    { provide: HTTP_INTERCEPTORS, useClass: ApiInterceptor, multi: true },
    {provide: LocationStrategy, useClass: HashLocationStrategy}
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
