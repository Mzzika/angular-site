import { TranslationService } from './services/translation.service';
import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { environment } from 'environments/environment';
import { versionNo } from './credentials/credentials';
import { MatomoInjector, MatomoTracker } from 'ngx-matomo';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  title = 'app works ;) ';
  version: string = versionNo;

  constructor(private translate: TranslateService, private matomoInjector: MatomoInjector, private matomoTracker: MatomoTracker, private translation: TranslationService) {
   }

  ngOnInit() {
    this.translations()
    this.matomo()
  }

  translations() {
    const language = this.translate.getBrowserLang();
    this.translation.setLanguage(language).then(() => {
  })
    
  }


  matomo() {

    this.matomoInjector.init('https://stats.HamzaSite.net:8443/piwik/', (environment.name == 'alpha') ? 108 : (environment.name == 'beta') ? 109 : (environment.name == 'production') ? 107 : 110);

    if (environment.name == 'alpha') {
      this.matomoTracker.setCookieDomain("*.discoveryalpha.HamzaSite.net")
    } else if (environment.name == 'beta') {
      this.matomoTracker.setCookieDomain("*.discoverybeta.HamzaSite.net")
    } else if (environment.name == 'production') {
      this.matomoTracker.setCookieDomain("*.discovery.HamzaSite.net")
    } else {
      this.matomoTracker.setCookieDomain("dev.HamzaSite.test")
    }

  }


}
