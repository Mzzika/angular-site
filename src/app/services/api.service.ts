import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import {
  requestHeaders, loginStatusEndPointUrl, meEndPointUrl, passwordEndPointUrl,
  loginEndPointUrl, signUpEndPointUrl, sendInviteEndPointUrl, acceptPrivateInviteEndPointUrl,
  acceptPublicInviteEndPointUrl, usersEndPointUrl, companiesEndPointUrl, connectorsEndPointUrl,
  companyConnectorsEndPointUrl, subscriptionsEndPointUrl, searchEndPointUrl, groupsEndPointUrl,
  logoutEndPointUrl, invitationsEndPointUrl, domainsEndPointUrl, versionNo
} from '../credentials/credentials';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { Observable } from 'rxjs';

@Injectable()
export class ApiService {

  error: boolean;
  errorMessage: string;
  loggedIn: boolean;

  constructor(private http: HttpClient) { }

  loginStatus() {
    return this.http.get(loginStatusEndPointUrl, { headers: requestHeaders })
      .catch(error => Observable.throw(error))
  }

  createUser(data) {
    return this.http.post(usersEndPointUrl, data, { headers: requestHeaders })
      .catch(error => Observable.throw(error))
  }

  updateUser(data) {
    return this.http.put(usersEndPointUrl + data.userId + '/', data, { headers: requestHeaders })
      .catch(error => Observable.throw(error))
  }

  updateSubscriber(data) {
    return this.http.put(subscriptionsEndPointUrl + data.userId + '/', JSON.stringify({ "user": data.user }), { headers: requestHeaders })
      .catch(error => Observable.throw(error))
  }

  getVersionNo() {
    return versionNo;
  }

  search(query) {
    let searchUrl = searchEndPointUrl + query;
    return this.http.get(searchUrl, { headers: requestHeaders })
      .catch(error => Observable.throw(error))
  }

  createDomain(domainInfo) {
    return this.http.post(domainsEndPointUrl, domainInfo, { headers: requestHeaders })
      .catch(error => Observable.throw(error))
  }

  updateDomain(domainInfo, domainUUID){
    return this.http.put(domainUUID, domainInfo, { headers: requestHeaders })
      .catch(error => Observable.throw(error))
  }

  deleteDomain(domainUUID){
    return this.http.delete(domainUUID, { headers: requestHeaders })
      .catch(error => Observable.throw(error))
  }

  checkPrivateInvitationKey(inviteKey) {
    // Check if a private invitation key is valid
    let acceptPrivateInviteUrl = acceptPrivateInviteEndPointUrl + inviteKey;
    return this.http.get(acceptPrivateInviteUrl, { headers: requestHeaders })
      .catch(error => Observable.throw(error))
  }

  checkPublicInvitationKey(inviteKey) {
    // Check if a public invitation key is valid
    let acceptPublicInviteUrl = acceptPublicInviteEndPointUrl + inviteKey;
    return this.http.get(acceptPublicInviteUrl, { headers: requestHeaders })
      .catch(error => Observable.throw(error))
  }

  getSubscriptionsAndInvitations() {
    return Observable.forkJoin(
      this.http.get(invitationsEndPointUrl, { headers: requestHeaders }),
      this.http.get(subscriptionsEndPointUrl, { headers: requestHeaders }))
      .catch(error => Observable.throw(error))
  }

  resendInvite(invitationUUID) {
    return this.http.get(invitationsEndPointUrl + '/' + invitationUUID + '/resend', { headers: requestHeaders })
      .catch(error => Observable.throw(error))
  }

  userLoginStatus() {
    this.loginStatus().subscribe(() => { this.loggedIn = true; }, error => { return this.loggedIn = false; })
    return this.loggedIn;
  }

  checkTokenAuthenticity() {
    return this.http.get(meEndPointUrl, { headers: requestHeaders })
      .catch(error => Observable.throw(error))
  }

  listUsers() {
    return this.http.get(usersEndPointUrl, { headers: requestHeaders })
      .catch(error => Observable.throw(error))
  }

  listSubscriptions() {
    return this.http.get(subscriptionsEndPointUrl, { headers: requestHeaders })
      .catch(error => Observable.throw(error))
  }

  listCompanies() {
    return this.http.get(companiesEndPointUrl, { headers: requestHeaders })
      .catch(error => Observable.throw(error))
  }

  listGroups() {
    return this.http.get(groupsEndPointUrl, { headers: requestHeaders })
      .catch(error => Observable.throw(error))
  }

  listAllConnectors() {
    let params = new HttpParams();
    params.set('publisher', 'false')

    return this.http.get(connectorsEndPointUrl, { params: params, headers: requestHeaders })
      .catch(error => Observable.throw(error))
  }

  addConnector(connectorInfo) {
    return this.http.post(companyConnectorsEndPointUrl, connectorInfo, { headers: requestHeaders })
      .catch(error => Observable.throw(error))
  }

  deleteConnector(connectorId) {
    return this.http.delete(companyConnectorsEndPointUrl + connectorId + '/', { headers: requestHeaders })
      .catch(error => Observable.throw(error))
  }

  listCompanyConnectors() {
    return this.http.get(companyConnectorsEndPointUrl + '?onlyMyCompany=true', { headers: requestHeaders })
      .catch(error => Observable.throw(error))
  }

  enabledDisabledConnectors(companyId) {
    let params = new HttpParams().set('publisher', 'false');

    return Observable.forkJoin(
      this.http.get(connectorsEndPointUrl, { params: params, headers: requestHeaders }),
      this.http.get(companiesEndPointUrl + companyId, { headers: requestHeaders }))
      .catch(error => Observable.throw(error))
  }

  makeLogin(email, password) {
    const credentials = JSON.stringify({ "email": email, "password": password });
    return this.http.post(loginEndPointUrl, credentials, { headers: requestHeaders })
      .catch(error => Observable.throw(error))
  }

  logOut() {
    return this.http.post(logoutEndPointUrl, {}, { headers: requestHeaders })
      .catch(error => Observable.throw(error))
  }

  createUserFromInvite(fullName, password, inviteKey, inviteType, email) {
    let acceptInviteUrl, credentials;

    if (inviteType == 'publicInvite') {
      credentials = JSON.stringify({ "first_name": fullName, "password": password, "email": email });
      acceptInviteUrl = acceptPublicInviteEndPointUrl + inviteKey + '/';
    } else {
      credentials = JSON.stringify({ "fullname": fullName, "new_password": password });
      acceptInviteUrl = acceptPrivateInviteEndPointUrl + inviteKey + '/';
    }

    return this.http.post(acceptInviteUrl, credentials, { headers: requestHeaders })
      .catch(error => Observable.throw(error))
  }

  changePassword(uid, tokenId, newPassword) {
    const credentials = JSON.stringify({ "new_password": newPassword, "token": tokenId, "uid": uid });
    return this.http.post(passwordEndPointUrl + 'confirm', credentials, { headers: requestHeaders })
      .catch(error => Observable.throw(error))
  }

  requestPassword(email) {
    const userEmail = JSON.stringify({ "email": email });
    return this.http.post(passwordEndPointUrl + 'reset', userEmail, { headers: requestHeaders })
      .catch(error => Observable.throw(error))
  }

  inviteUser(emailAddresses) {
    const inviteDetails = JSON.stringify({ "invite_user_email_list": emailAddresses, "inviter_email": "support@HamzaSite.com" });

    return this.http.post(sendInviteEndPointUrl, inviteDetails, { headers: requestHeaders })
      .catch(error => Observable.throw(error))
  }

  getCompanyInformation(companyId) {
    return this.http.get(companiesEndPointUrl + companyId, { headers: requestHeaders })
      .catch(error => Observable.throw(error))
  }

  numberOfRemainingLicenses(companyId) {
    return this.http.get(companiesEndPointUrl + companyId + '/num_free_subscriptions', { headers: requestHeaders })
      .catch(error => Observable.throw(error))
  }

  updateCompanyInformation(information, companyId) {
    const updatedInformation = JSON.stringify(information);
    return this.http.put(companiesEndPointUrl + companyId + '/', updatedInformation, { headers: requestHeaders })
      .catch(error => Observable.throw(error))
  }

  changeStatus(companyUuid, status) {
    return this.http.put(companyConnectorsEndPointUrl + companyUuid + '/', status, { headers: requestHeaders })
      .catch(error => Observable.throw(error))
  }

  updateCompanyConnector(companyUuid, data) {
    return this.http.put(companyConnectorsEndPointUrl + companyUuid + '/', data, { headers: requestHeaders })
      .catch(error => Observable.throw(error))
  }

  changeUserStatus(userUuid, status) {
    const userStatus = JSON.stringify(status);
    return this.http.patch(usersEndPointUrl + userUuid + '/', userStatus, { headers: requestHeaders })
      .catch(error => Observable.throw(error))
  }

  changeSubscriberStatus(userUuid, status) {
    const userStatus = JSON.stringify(status);
    return this.http.put(usersEndPointUrl + userUuid + '/subscriber_active_status/', userStatus, { headers: requestHeaders })
      .catch(error => Observable.throw(error))
  }

  createConnector(info) {
    const information = JSON.stringify(info);
    return this.http.post(companyConnectorsEndPointUrl, information, { headers: requestHeaders })
      .catch(error => Observable.throw(error))
  }

  connectorStatus(connectorName) {
    return this.http.get(connectorsEndPointUrl + connectorName + '/status', { headers: requestHeaders })
      .catch(error => Observable.throw(error))
  }

  createCompany(information) {
    const updatedInformation = JSON.stringify(information);
    return this.http.post(companiesEndPointUrl, updatedInformation, { headers: requestHeaders })
      .catch(error => Observable.throw(error))
  }

  changeUserConnectorStatus(userUuid, companyConnectorUuid, status) {
    const information = {
      company_connector_uuid: companyConnectorUuid,
      user_uuid: userUuid
    };
    JSON.stringify(information)
    return this.http.post(companyConnectorsEndPointUrl + status, information, { headers: requestHeaders })
      .catch(error => Observable.throw(error));
  }

}
