import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class TranslationService {
  data: any = {};
  constructor(private http: HttpClient) {
    }
    setLanguage(lang: string): Promise<{}>{
      // Set headers to ignore http interceptor.
      const headers = new HttpHeaders().set('X-Skip-Interceptor', '');
      return new Promise<{}>((resolve, reject) => {
        const langPath = `assets/i18n/${lang}.json`;
        this.http.get<{}>(langPath, {headers: headers}).subscribe(
          translation => {
            this.data = translation
            resolve(this.data);
          },
          error => {
            console.log('Language file not found resolving to English ...')
            // Set language file to en if language file not found
            this.http.get<{}>(`assets/i18n/en.json`, {headers: headers}).subscribe(
              translation => {
                this.data = translation
                resolve(this.data);
              },
            );
          }
        );
      });
    }
  }