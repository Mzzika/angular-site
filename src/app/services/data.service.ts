import { Injectable } from '@angular/core';

@Injectable()
export class DataService {

  private authData;
  private message;

  constructor() { }

  getAuthData() {
    return this.authData;
  }
  setAuthData(data) {
    this.authData = data;
  }

  getMessage() {
    return this.message;
  }
  setMessage(data) {
    this.message = data;
  }

}
