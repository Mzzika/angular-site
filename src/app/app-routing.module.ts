import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginPageComponent } from './components/login-page/login-page.component';
import { SignupComponent } from './components/signup/signup.component';
import { ChangePasswordComponent } from './components/change-password/change-password.component'
import { ForgotPasswordComponent } from './components/forgot-password/forgot-password.component'
import { SuperAdminHomeComponent } from './components/super-admin-home/super-admin-home.component'
import { SalesAdminHomeComponent } from './components/sales-admin-home/sales-admin-home.component'
import { ClientAdminHomeComponent } from './components/client-admin-home/client-admin-home.component'
import { CreateUserComponent } from './components/create-user/create-user.component'
import { CreateClientComponent } from './components/create-client/create-client.component'
import { InviteUsersComponent } from './components/invite-users/invite-users.component'
import { AcceptInviteComponent } from './components/accept-invite/accept-invite.component'
import { SearchComponent } from './components/search/search.component'
import { ConnectorsComponent } from './components/connectors/connectors.component'
import { DomainsComponent } from './components/domains/domains.component'
import { AddDomainComponent } from './components/add-domain/add-domain.component'
import { UpdateDomainComponent } from './components/update-domain/update-domain.component'
import { CompanyConnectorsSettingsComponent } from './components/company-connectors-settings/company-connectors-settings.component'
import { UpdateClientComponent } from './components/update-client/update-client.component';
import { UpdateUserComponent } from './components/update-user/update-user.component';
import { UpdateSubscriberComponent } from './components/update-subscriber/update-subscriber.component';
import { RoleComponent } from './components/role/role.component';
import { AuthGuard } from './auth.guard';
import { Page404Component } from './components/page-404/page-404.component';
import { Page500Component } from './components/page-500/page-500.component';

const routes: Routes = [
  {
    path: '',
    component: RoleComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'admin/super-admin/users/update',
    component: UpdateUserComponent
  },
  {
    path: 'admin/company/subscribers/update',
    component: UpdateSubscriberComponent
  },
  {
    path: 'admin/login',
    component: LoginPageComponent,
    data: { isSubscriber: true }
  },
  {
    path: 'connectors',
    component: ConnectorsComponent
  },
  {
    path: 'admin/sales/companies/:companyId/domains',
    component: DomainsComponent
  },
  {
    path: 'admin/sales/companies/:companyId/domains/add',
    component: AddDomainComponent
  },
  {
    path: 'admin/sales/companies/:companyId/domains/update',
    component: UpdateDomainComponent
  },
  {
    path: 'admin/sales/companies/:companyId/connectors',
    component: CompanyConnectorsSettingsComponent
  },
  {
    path: 'reset-password/:uid/:token',
    component: ChangePasswordComponent
  },
  {
    path: 'forgot-password',
    component: ForgotPasswordComponent
  },
  {
    path: 'admin/sales/companies',
    component: SalesAdminHomeComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'admin/sales/companies/:companyId/update',
    component: UpdateClientComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'admin/company/subscribers',
    component: ClientAdminHomeComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'admin/super-admin/users',
    component: SuperAdminHomeComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'signUp',
    component: SignupComponent
  },
  {
    path: 'admin/super-admin/users/add',
    component: CreateUserComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'admin/sales/companies/add',
    component: CreateClientComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'admin/company/subscribers/invite',
    component: InviteUsersComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'invitations/accept/:invitationKey',
    data: { inviteType: 'privateInvite' },
    component: AcceptInviteComponent
  },
  {
    path: 'client/invite-subscriptors/:invitationKey',
    data: { inviteType: 'publicInvite' },
    component: AcceptInviteComponent
  },
  {
    path: 'admin/home',
    component: RoleComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'subscriber/home',
    component: RoleComponent,
    data: { isSubscriber: true },
    canActivate: [AuthGuard]
  },
  {
    path: 'view-all-results/:query',
    component: SearchComponent,
    data: { isSubscriber: true },
    canActivate: [AuthGuard]
  },
  {
    path: '404',
    component: Page404Component
  },
  {
    path: 'error',
    component: Page500Component
  },
  {
    path: '**',
    redirectTo: '/404'
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
