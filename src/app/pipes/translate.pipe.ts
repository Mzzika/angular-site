import { Pipe, PipeTransform } from '@angular/core';
import { TranslationService } from '../services/translation.service';

@Pipe({
  name: 'translate',
  pure: false
})
export class TranslatePipe implements PipeTransform {

  constructor(private translate: TranslationService) {}
  transform(key: any): any {
    let k0 = key.split('.')[0];
    let k1 = key.split('.')[1];
    return this.translate.data[k0][k1] || key;
  };
}
