import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { environment } from 'environments/environment';


const apiVersion = "api/v1.0/"
export const baseUrl = environment.serverUrl + apiVersion;

export const APIKey = environment.apiKey;

export const versionNo = '3.1.0';

export const requestHeaders = new HttpHeaders().set('Content-Type', 'application/json').set('API-Key', APIKey);

export const passwordEndPointUrl = baseUrl + "auth/password/"
export const loginEndPointUrl = baseUrl + "auth/login/"
export const meEndPointUrl = baseUrl + "auth/me/"
export const logoutEndPointUrl = baseUrl + "auth/logout/"
export const signUpEndPointUrl = baseUrl + "auth/signup/"
export const invitationsEndPointUrl = baseUrl + "invitations"
export const sendInviteEndPointUrl = baseUrl + "invitations/send/"
export const usersEndPointUrl = baseUrl + "users/"
export const companiesEndPointUrl = baseUrl + "companies/"
export const acceptPrivateInviteEndPointUrl = baseUrl + "invitations/accept/"
export const acceptPublicInviteEndPointUrl = companiesEndPointUrl + "invite-subscribers/"
export const connectorsEndPointUrl = baseUrl + "connectors/"
export const domainsEndPointUrl = baseUrl + "domains/"
export const companyConnectorsEndPointUrl = baseUrl +  "company_connectors/"
export const subscriptionsEndPointUrl = baseUrl + "subscriptions/"
export const searchEndPointUrl = baseUrl + "search/?q="
export const groupsEndPointUrl = baseUrl + "groups/"
export const loginStatusEndPointUrl = baseUrl + "login_status/"


@Injectable()
export class CredentialsService {

  constructor() { }

}
