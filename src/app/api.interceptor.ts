import { Injectable } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpResponse,
  HttpErrorResponse,
} from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';

// Set interceptor to skip certain request , e.g i18n files.
export const InterceptorSkipHeader = 'X-Skip-Interceptor';

@Injectable()
export class ApiInterceptor implements HttpInterceptor {

  constructor(private route: ActivatedRoute, private router: Router) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (request.headers.has(InterceptorSkipHeader)) {
      const headers = request.headers.delete(InterceptorSkipHeader);
      return next.handle(request.clone({ headers }));
    }
    /*
        Don't intercept HTTP requests for Public and Private Invites,
        We have to use window.location.hash to Get the current URL because we can't
        access the current route (i.e "this.route") in the HTTP interceptor
      */
    const allowedRoutes = ['invite-subscriptors', 'invitations']
    const urlParams = window.location.hash.split('/')

    for(let i = 0; i < allowedRoutes.length; i++) {
      if (urlParams.indexOf(allowedRoutes[i]) > -1) {
        return next.handle(request)
      }
    }

    return next.handle(request).do((event: HttpEvent<any>) => {
      if (event instanceof HttpResponse) {
        // Do stuff with General responses (E.g add headers)
      }
    }, (err: any) => {
      if (err instanceof HttpErrorResponse) {
        if (err.status === 401) {
          // redirect the user back to the login page
          this.router.navigate(['/admin/login'])
        }
        if (err.status === 500 || err.status === 502 ) {
          this.router.navigate(['/error'])
        }
        if (err.status === 404 ) {
          this.router.navigate(['/404'])
        }
      }
    });
  }
}
