import { Component, OnInit } from '@angular/core';
import { Title, Meta } from '@angular/platform-browser';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiService } from '../../services/api.service';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit {

  email: any
  password: any
  error: boolean;
  errorMessage: string;
  versionNo: string;
  isSubscriber: boolean;

  constructor(private title: Title, private meta: Meta, private api: ApiService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    // Everytime the page is loaded, check if the user is logged in and redirect them

    this.meta.addTag({ name: 'robots', content: 'noindex' });

    this.route.data.subscribe(data => this.isSubscriber = data.isSubscriber)

    if (this.isSubscriber) {
      this.title.setTitle('Login - HamzaSite Discovery');
    } else {
      this.title.setTitle('Admin login - HamzaSite Discovery');
    }

    this.checkLoginStatus();

  }

  login() {
    if (this.email && this.password) {
      this.api.makeLogin(this.email, this.password).subscribe(data => {
        this.router.navigate(['/admin/home'])
      }, error => {
        this.error = true;
        this.errorMessage = error.error.error;
      });
    } else {
      this.error = true;
      this.errorMessage = 'Please fill all the fields'
    }
  }

  checkLoginStatus() {
    return this.api.checkTokenAuthenticity().subscribe(data => {
      this.router.navigate(['/admin/home'])
    }, error => {
      return; 
    })
  }

}
