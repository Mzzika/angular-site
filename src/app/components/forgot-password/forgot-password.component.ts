import { Component, OnInit } from '@angular/core';
import { Title, Meta } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { ApiService } from '../../services/api.service';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {

  email: string = '';
  errorMessage: string = '';
  emailSent: boolean = false;
  error: boolean;
  successMessage:string = 'Email successfully sent!'

  constructor(private title: Title, private api: ApiService, private router: Router, private meta:Meta) { }

  ngOnInit() {
    this.meta.addTag({ name: 'robots', content: 'noindex' });
    this.title.setTitle('Forgot password? - HamzaSite Discovery')
  }

  resetPassword() {
    if (this.email.length > 0) {

      this.api.requestPassword(this.email).subscribe((res) => {
        this.emailSent = true;
        this.error = false;
      }, error => {
        console.log('err ', error)
        this.error = true;
        this.errorMessage = error.error.error_list[0].errors;
      })

    } else {
      this.error = true;
      this.errorMessage = 'Please write the email attached to your account'
    }
  }

}
