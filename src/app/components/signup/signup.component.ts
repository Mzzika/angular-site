import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ApiService } from '../../services/api.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {

  email: any
  password: any
  fullName: string;
  error: boolean;
  errorMessage: string;
  passwordConfirmation: string;
  userRegistredSuccessfully:boolean = false;

  constructor(private title: Title, private api: ApiService, private router: Router) { }

  ngOnInit() {
    this.title.setTitle('Sign up - HamzaSite Discovery');
    this.checkAuthencity()
  }

  checkAuthencity() {
    // check user login status and redirect if the user is logged in
    return this.api.checkTokenAuthenticity().subscribe(data => {
      this.router.navigate(['/admin/home'])
    })
  }

  signUp() {
    if (!this.email || !this.fullName || !this.password || !this.passwordConfirmation) {
      this.error = true;
      this.errorMessage = 'Please fill all the fields!'
      return
    } else {
      if (this.password !== this.passwordConfirmation) {
        this.error = true;
        this.errorMessage = 'Passwords do not match!';
        return;
      }
      this.userRegistredSuccessfully = true;

      //Signup service
      // this.api.createUser(this.fullName, this.email, this.password).subscribe(data => {
      //    console.log('Success, auth token is : ', JSON.parse(data._body).auth_token);
      // }, err => {
      //   this.error = true;
      //   this.errorMessage = JSON.parse(err._body).non_field_errors[0]
      // });
    }


    // if (this.email && this.fullName && this.password && this.passwordConfirmation) {

    //   if (this.password === this.passwordConfirmation) {

    //     this.userRegistredSuccessfully = true;

    //     //Signup service
    //     // this.api.createUser(this.fullName, this.email, this.password).subscribe(data => {
    //     //    console.log('Success, auth token is : ', JSON.parse(data._body).auth_token);
    //     // }, err => {
    //     //   this.error = true;
    //     //   this.errorMessage = JSON.parse(err._body).non_field_errors[0]
    //     // });

    //   } else {
    //     this.error = true;
    //     this.errorMessage = 'Passwords are not matching!'
    //   }
    // } else {
    //   this.error = true;
    //   this.errorMessage = 'Please fill all the fields!'
    // }
  }
}
