import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ApiService } from '../../services/api.service';
import { DataService } from '../../services/data.service';
import { Location } from '@angular/common';
import { Router } from '@angular/router';

@Component({
  selector: 'app-super-admin-home',
  templateUrl: './super-admin-home.component.html',
  styleUrls: ['./super-admin-home.component.scss']
})
export class SuperAdminHomeComponent implements OnInit {

  users: Array<any>
  publicInviteUrl: string;
  successMessage: string;
  public loading: boolean = false;

  constructor(private title: Title, private api: ApiService, private data: DataService, private location: Location, private router: Router) { }

  ngOnInit() {
    this.title.setTitle('User list - Admin - HamzaSite Discovery');
    this.usersList();
    this.companyInviteLink();
    if(this.data.getMessage()){
      this.successMessage = this.data.getMessage();
    }
  }

  ngOnDestroy(){
    this.data.setMessage(null)
  }

  updateUser(userData) {
    this.router.navigate(['/admin/super-admin/users/update'])
    this.data.setAuthData(userData);
  }

  companyInviteLink() {
    this.api.checkTokenAuthenticity().subscribe(data => {
      this.publicInviteUrl = window.location.origin + '/app#/client/invite-subscriptors/' + data.company.uuid;
      return this.publicInviteUrl;
    }, error => {
      console.log('err ', error)
    })
  }

  usersList() {
    this.loading = true;
    return this.api.listUsers().subscribe(users => {
      this.users = users;
      console.log('us ', users)
      this.loading = false;
    })
  }

  changeStatus(user) {
    if (user.is_active == false) {
      user.is_active = true;
      this.api.changeUserStatus(user.uuid, { 'is_active': true }).subscribe(() => {}, error => {
        console.log('err ', error)
      })

    } else if (user.is_active == true) {
      user.is_active = false;
      this.api.changeUserStatus(user.uuid, { 'is_active': false }).subscribe(() => {}, error => {
        console.log('err ', error)
      })
    }
  }

}
