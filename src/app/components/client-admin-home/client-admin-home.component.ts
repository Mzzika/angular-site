import { Component, OnInit, TemplateRef } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router } from "@angular/router";
import { ModalModule } from 'ngx-bootstrap/modal';
import { ApiService } from '../../services/api.service';
import { DataService } from '../../services/data.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { Location } from '@angular/common';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

@Component({
  selector: 'app-client-admin-home',
  templateUrl: './client-admin-home.component.html',
  styleUrls: ['./client-admin-home.component.scss']
})
export class ClientAdminHomeComponent implements OnInit {

  subscribers: any = []
  publicInviteUrl: string;
  invitationsArray: any = [];
  sent: boolean = false;
  successMessage: string;
  modalRef: BsModalRef;
  copied: boolean = false;

  constructor(private title: Title, private api: ApiService, private data: DataService,
    private route: Router, private modalService: BsModalService, private location: Location) { }

  ngOnInit() {
    this.title.setTitle('Subscribers list - HamzaSite Discovery');
    this.subscriptionsInvitations();
    this.companyInviteLink()
  }

  goBack() {
   this.location.back();
 }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, { class: 'share-link-modal' });
  }

  resendInvite(inviteUUID) {
    return this.api.resendInvite(inviteUUID).subscribe(data => {
      this.sent = data.status;
      this.successMessage = data.message;
    })
  }

  copyLink() {
    const event = (ClipboardEvent) => {
      ClipboardEvent.clipboardData.setData('text/plain', this.publicInviteUrl);
      ClipboardEvent.preventDefault();
      document.removeEventListener('copy', ClipboardEvent);
    }
    document.addEventListener('copy', event);
    document.execCommand('copy');
    this.copied = true;
  }

  updateSubscriber(user) {
    this.data.setAuthData(user);
    this.route.navigate(['admin/company/subscribers/update'])
  }


  subscriptionsInvitations() {
    return this.api.getSubscriptionsAndInvitations().subscribe(([res1, res2]) => {

      res2.forEach(res => {

        res1.forEach(data => (res.user.url == data.new_user) ? res.invitationInfo = data : null)

        this.subscribers.push(res);

      })

    })
  }


  companyInviteLink() {
    this.api.checkTokenAuthenticity().subscribe(data => {
      this.publicInviteUrl = window.location.origin + '/app#/client/invite-subscriptors/' + data.company.uuid;
      return this.publicInviteUrl;
    }, error => {
      console.log('err ', error)
    })
  }

  changeStatus(user) {
    if (user.status == 'inactive') {
      user.status = 'active'
      this.api.changeSubscriberStatus(user.user.uuid, { 'is_active': true }).subscribe(result => {
        user.status = 'active';
        console.log('result ', result)
      }, error => {
        console.log('err ', error)
      })

    } else if (user.status == 'active') {
      user.status = 'inactive'
      this.api.changeSubscriberStatus(user.user.uuid, { 'is_active': false }).subscribe(result => {
        user.status = 'inactive';
        console.log('result ', result)
      }, error => {
        console.log('err ', error)
      })
    }
  }

}
