import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SalesAdminHomeComponent } from './sales-admin-home.component';

describe('SalesHomeComponent', () => {
  let component: SalesAdminHomeComponent;
  let fixture: ComponentFixture<SalesAdminHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SalesAdminHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SalesAdminHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
