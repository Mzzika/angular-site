import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ApiService } from '../../services/api.service';
import { DataService } from '../../services/data.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-sales-admin-home',
  templateUrl: './sales-admin-home.component.html',
  styleUrls: ['./sales-admin-home.component.scss']
})
export class SalesAdminHomeComponent implements OnInit {

  companies: Array<any>;
  successMessage:string;
  loading: boolean = false;


  constructor(private title:Title, private api:ApiService, private data: DataService, private location: Location) { }


  ngOnInit() {
    this.title.setTitle('Companies list - Sales - HamzaSite Discovery');
    this.companiesList()
    if (this.data.getMessage()) {
      this.successMessage = this.data.getMessage();
    }
  }

  cleanService(){
    return this.data.setMessage(null);
  }

  companiesList(){
    this.loading = true;
    return this.api.listCompanies().subscribe(companies=>{
      this.companies = companies;
      this.loading = false;
    })
  }

}
