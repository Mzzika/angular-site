import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiService } from '../../services/api.service';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-update-client',
  templateUrl: './update-client.component.html',
  styleUrls: ['./update-client.component.scss']
})
export class UpdateClientComponent implements OnInit {

  connectors: string;
  companyData: Array<any>;
  conpanyName: any;
  password: string = '';
  noOfLicenses: number;
  companyId: any;
  error: boolean;
  errorMessage: string;
  successMessage:string;
  errorList:any = [];


  constructor(private title: Title, private api: ApiService, private route: ActivatedRoute, private data: DataService, private router:Router) { }

  ngOnInit() {
    this.companyId = this.route.snapshot.paramMap.get('companyId');
    this.companyInformation(this.companyId)
  }

  companyInformation(companyId) {
    this.api.getCompanyInformation(this.companyId).subscribe(data => {
      this.companyData = data;
      this.conpanyName = data.name;
      this.noOfLicenses = data.num_subscriptions;
      this.title.setTitle(`Update ${data.name} - Sales - HamzaSite Discovery`);
      this.connectors = data.connectors;
    })
  }

  updateInformation() {
    if (this.conpanyName && (this.password || this.noOfLicenses)) {

      let clientDetails = {
        name: this.conpanyName,
        num_subscriptions: this.noOfLicenses,
        password: (this.password.length > 0 ? this.password : undefined)
      }


      this.api.updateCompanyInformation(clientDetails, this.companyId).subscribe(data => {

        this.router.navigate(['admin/sales/companies'])
        this.data.setMessage('Update successfully done!')
        this.error = false;

      }, error => {
        this.error = true;
        this.errorMessage = error.error.error;
        this.errorList = error.error.error_list;
      })
    } else {
      this.error = true;
      this.errorMessage = 'In order to update this client, you need to update one of the values below'
    }
  }
}
