import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ApiService } from '../../services/api.service';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-connectors',
  templateUrl: './connectors.component.html',
  styleUrls: ['./connectors.component.scss']
})
export class ConnectorsComponent implements OnInit {

  companyconnectors: any;
  errorMessage: string;
  loading: boolean = false;
  userData: any;
  constructor(private title: Title, private api: ApiService, private data: DataService) { }

  ngOnInit() {
    this.title.setTitle('Connectors - HamzaSite Discovery');
    this.getCompanyId();
    this.getUserData();
  }

  getUserData() {
    return this.api.checkTokenAuthenticity().subscribe(data => {
      this.userData = data;
    })
  }

  getCompanyId() {
    this.api.checkTokenAuthenticity().subscribe(() => {
      this.allConnectors();
    }, error => {
      console.log('error ', error)
    })
  }

  allConnectors() {
    this.loading = true;
    this.api.listCompanyConnectors().subscribe(data => {
      this.companyconnectors = data;
      this.loading = false;
    }, error => {
      console.log('err ', error)
    })
  }

  toggleConectorStatus(companyConnector) {
    if (companyConnector.enabled == true) {
      companyConnector.enabled = false;
      this.api.changeUserConnectorStatus(this.userData.uuid, companyConnector.uuid, 'disable').subscribe(() => {}, error => {
        console.log('err ', error)
      });
    } else  if (companyConnector.enabled == false){
      companyConnector.enabled = true;
      this.api.changeUserConnectorStatus(this.userData.uuid, companyConnector.uuid, 'enable').subscribe(() => {}, error => {
        console.log('err ', error)
      });
    }
  }

}
