import { Component, OnInit } from '@angular/core';
import { Title, Meta } from '@angular/platform-browser';
import { Socket } from 'ng-socket-io';
import { ActivatedRoute } from "@angular/router";
import { ApiService } from '../../services/api.service';


@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})

export class SearchComponent implements OnInit {
  connected: boolean = false;
  connectorCount: number = 0;
  connectorsArray: string[] = [];
  defaultSelectionValue: string;
  errorMessage: string = '';
  filteredResults: any[] = [];
  finalConnectorCount: number = 0;
  notLoggedInErrorMessages: string = '';
  notLoggedInConnectors: string[] = []
  numberOfResults: number = 0;
  results: any[] = [];
  searchIndex: number = 0;
  searching: boolean = false;
  searchLimit: number = 500;
  searchMaxResults: number = 0;
  searchQuery: string = '';
  searchFormString: string;
  selectConnector: string;
  showAllResults: string = 'showAllResults';
  showThumb:boolean = false;


  constructor(private title: Title, private meta: Meta, private api: ApiService, private socket: Socket, private route: ActivatedRoute) { }

  ngOnInit() {
    this.meta.addTag({ name: 'robots', content: 'noindex' });

    this.title.setTitle('View all results - HamzaSite Discovery');

    this.route.params.subscribe(params => this.getSearch(params));

    this.createSockets();
  }

  resetData() {
    // Reset data anytime the route changes
    this.connectorsArray = []
    this.connectorCount = 0;
    this.filteredResults = []
    this.numberOfResults = 0;
    this.results = []
  }


  getSearch(query) {
    this.searchQuery = query.query;
    this.resetData()
    this.api.search(this.searchQuery).subscribe((res) => {
      this.joinRoom(res);
    }, (err) => {
      console.log('err ', err);
    })
  }

  selectedConnector(connector) {
    if (connector != 'showAllResults') {
      this.filteredResults = this.results.filter(result => result.connector == connector)
      this.connectorCount = 1;
      this.numberOfResults = this.filteredResults.length;
    } else {
      //Show 'All results'
      this.filteredResults = this.results;
      this.connectorCount = this.finalConnectorCount
      this.numberOfResults = this.filteredResults.length;
    }
  }

  createSockets() {
    this.socket.on('connect', () => {
      this.connected = true;
      this.socket.emit('client connected', { data: 'I\'m connected!' })
    })

    this.socket.on('disconnect', () => {
      this.connected = false;
    })

    this.socket.on('not_logged_in', (response) => {
      console.log('response ', response)
    })

    this.socket.on('search_result', (response) => {
      let connectorCount = this.connectorCount + 1
      this.results = [...this.results, ...response.results];
      this.numberOfResults = this.results.length;
      this.connectorCount = connectorCount;
      // this.connectorsArray.push(response.connector);
      this.setSearchDisplayResults()
    });

    this.socket.on('search_completed', (response) => {
      console.log('search_completed', response.results)
      this.results = response.results;
      this.numberOfResults = this.results.length;
      this.connectorCount = response.num_connectors;
      this.finalConnectorCount = response.num_connectors;
      // Show only connectors with results.
      for (var i = 0; i < response.results.length; i++) {
        if (!(this.connectorsArray.indexOf(response.results[i].connector) > -1)) {
          this.connectorsArray.push(response.results[i].connector);
        }
        this.connectorsArray.sort(function (a, b) {
          return a.localeCompare(b);
        });
      }
      this.searching = false;
      this.setSearchDisplayResults()
    });

    this.socket.on('not_logged_in', (response) => {
      this.notLoggedInConnectors.push(response.connector)
      const x = this.notLoggedInConnectors;  // to make the code shorter
      if (x.length > 1) {
        const joinedConnectors = `${x.slice(0, -1).join(',')} and ${x[x.length-1]}`;
        this.notLoggedInErrorMessages = `You are not logged in to: ${joinedConnectors}`;
      } else {
        this.notLoggedInErrorMessages = `You are not logged in to: ${x[0]}`;
      }
    })
  }

  joinRoom(response) {
    this.searching = true;
    return this.socket.emit('join', response.search_id)
  }

  setSearchDisplayResults() {
    const results = this.results.slice(this.searchIndex).slice(0, this.searchLimit)
    this.filteredResults = results;
    console.log('results ', results)
  }

  pluralize(count, noun, suffix='s') {
    if(count) {
      return `From ${count} ${noun}${count !== 1 ? suffix : ''}`;
    }
  }

  numConnectors() {
    return this.pluralize(this.connectorCount, 'Connector')
  }

  showThumbnail(){
    if(this.showThumb){
      this.showThumb = false;
    }else{
      this.showThumb = true;
    }
  }
  // Do search when search button is clicked.
  doSearch() {
    this.notLoggedInConnectors.length = 0
    this.searchQuery = this.searchFormString;
    this.resetData();
    this.api.search(this.searchQuery).subscribe((res) => {
      this.joinRoom(res);
    }, (err) => {
      console.log('err ', err);
    })
  }

}
