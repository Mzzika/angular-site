import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiService } from '../../services/api.service';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {

  oldPassword = '';
  password = '';
  password2 = '';
  error: boolean;
  errorMessage = ''
  uid: any;
  tokenId: any;
  passwordChanged:boolean = false;

  constructor(private api: ApiService, private router: Router, private title: Title, private route: ActivatedRoute) { }

  ngOnInit() {
    this.title.setTitle('Change password - HamzaSite Discovery');
    this.uid = this.route.snapshot.paramMap.get('uid');
    this.tokenId = this.route.snapshot.paramMap.get('token');
  }

  changePassword() {


    if (this.password && this.password2) {
      if (this.password === this.password2) {

        this.api.changePassword(this.uid, this.tokenId, this.password).subscribe(data => {
          console.log('Success : ', data);
          this.error = false;
          this.passwordChanged = true;
        }, error => {
          this.error = true;
          console.log('err ', error)
          this.errorMessage = error.error.error_list[0].errors;
        });
      } else {
        this.error = true;
        this.errorMessage = 'The new passwords does not match';
      }
    } else {
      this.error = true;
      this.errorMessage = 'Please fill all the fields'
    }


  }

}
