import { Component, OnInit, HostListener } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { ApiService } from '../../services/api.service';
import * as _ from 'lodash'
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/debounceTime';

@Component({
  selector: 'app-invite-users',
  templateUrl: './invite-users.component.html',
  styleUrls: ['./invite-users.component.scss']
})

@HostListener('document:keyup', ['$event'])

export class InviteUsersComponent implements OnInit {

  error = false;
  errorMessage = ''
  emailsList;
  emailsNotSent;
  emailsSent;
  emailAddresses:string;
  successMessage = '';
  numberOfRemainingLicenses: any = '--';
  updatedNoOfLicenses = 0;
  showNumberOfLicenses = false;

  private subject: Subject<string> = new Subject();

  constructor(private title: Title, private api: ApiService) { }

  ngOnInit() {
    this.title.setTitle('Invite subscribers - HamzaSite Discovery');
    this.getCompanyId();
  }

  getCompanyId() {
    return this.api.checkTokenAuthenticity().subscribe(data => {
      this.getNumberOfLicenses(data.company.uuid)
    }, error => {
      this.error = true;
      this.errorMessage = error.detail;
    })
  }

  getNumberOfLicenses(companyId) {
    this.api.numberOfRemainingLicenses(companyId).subscribe(data => {
      console.log('data ', data.num_free_subscriptions)
      this.numberOfRemainingLicenses = data.num_free_subscriptions;
      this.invitedEmailsField(this.numberOfRemainingLicenses)
    }, error=>{
      console.log('error ', error)
    })
  }

  invitedEmailsField(numberOfFreeLicenses) {
    this.subject.debounceTime(350).subscribe(searchTextValue => {
      let repl = searchTextValue.split(' ').join('').split(',').filter(arr => arr);
      let validEmails = []

      repl.forEach(email => {
        if (this.validateEmail(email)) {
          validEmails.push(email);
        }
      })

      let finalEmailList = _.uniq(validEmails).slice(0, numberOfFreeLicenses)

      this.emailsList = finalEmailList;

      if (finalEmailList.length <= numberOfFreeLicenses) {
        this.showNumberOfLicenses = true;
        this.updatedNoOfLicenses = numberOfFreeLicenses - finalEmailList.length;

        if (this.updatedNoOfLicenses == 0) {
          this.numberOfRemainingLicenses = 0;
          this.showNumberOfLicenses = false;
        }
      }

    });
  }

  onKeyUp(ev) {
    this.subject.next(ev);
  }

  validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

  inviteUser() {
    this.emailsNotSent = ''
    this.emailsSent = ''
    this.error = false

    if (this.emailsList) {
      this.api.inviteUser(this.emailsList).subscribe(data => {
        this.error = false;
        if (data.data.emails_not_sent.length > 0) {
          this.emailsNotSent =data.data.emails_not_sent;
        }
        if (data.data.emails_sent.length > 0) {
          this.emailsSent = data.data.emails_sent;
        }

      }, error => {
        this.error = true;
        this.errorMessage = error.error.error;
        if (error.error.data.emails_not_sent.length > 0) {
          this.emailsNotSent = error.error.data.emails_not_sent;
        }
        if (error.error.data.emails_sent.length > 0) {
          this.emailsSent = error.error.data.emails_sent;
        }
      });
    } else {
      this.error = true;
      this.errorMessage = 'Please fill all the fields'
    }
  }

}
