import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from '../../services/api.service';
import { Title } from '@angular/platform-browser';
import { DataService } from '../../services/data.service';
import * as _ from 'lodash';

@Component({
  selector: 'app-domains',
  templateUrl: './domains.component.html',
  styleUrls: ['./domains.component.scss']
})
export class DomainsComponent implements OnInit {

  companyInfo: any;
  companyName: string = '--';
  companyuuid: string;
  loading: boolean = false;
  successMessage: string;

  constructor(private api: ApiService, private data: DataService,
    private activatedRoute: ActivatedRoute, private title: Title, private router: Router) { }

  ngOnInit() {
    this.companyuuid = this.activatedRoute.snapshot.paramMap.get('companyId');
    if (this.companyuuid) {
      this.getCompanyInfo();
    }
    if (this.data.getMessage()) {
      this.successMessage = this.data.getMessage();
    }
  }

  ngOnDestroy() {
    this.data.setMessage(null)
  }

  deleteDomain(domainUUID) {
    this.api.deleteDomain(domainUUID).subscribe(data => {
      _.remove(this.companyInfo.domains, domainURL => domainURL.url === domainUUID);
      this.successMessage = 'Domain deleted successfully!';
    }, error => {
      console.log('error ', error);
    })
  }

  getCompanyInfo() {
    this.loading = true;
    this.api.getCompanyInformation(this.companyuuid).subscribe(data => {
      this.companyInfo = data;
      this.loading = false;
      this.title.setTitle(`Domains list - Company ${data.name} - HamzaSite Discovery`)
    }, error => {
      console.log('err ', error)
    })
  }

}
