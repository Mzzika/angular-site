import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ApiService } from '../../services/api.service';
import { Router } from '@angular/router';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.scss']
})
export class CreateUserComponent implements OnInit {

  selectedRole = '';
  fullName: string = '';
  email: string = '';
  group:any = [];
  groups:any = [];
  company: any;
  error: boolean;
  errorMessage: string = ''
  successMessage: string = ''
  userCreatedSuccessfully = false;
  companies = [];
  errorList:any = [];

  constructor(private title: Title, private api: ApiService, private data:DataService, private route:Router) { }

  ngOnInit() {
    this.title.setTitle('Add user - Admin - HamzaSite Discovery');
    this.companiesList();
    this.groupsList();
  }

  selectedGroup(group) {
    this.group = [];
    this.group.push(group)
  }

  selectedCompany(company) {
    this.company = company;
  }

  createUser() {

    var formData = {
      "first_name": this.fullName,
      "email": this.email,
      "company": this.company,
      "groups": this.group
    }

    if (this.fullName && this.email) {

      this.api.createUser(formData).subscribe(data => {

        this.route.navigate(['admin/super-admin/users'])
        this.data.setMessage('User created successfully!')

        this.error = false;

      }, error => {
        this.error = true;
        this.errorMessage = error.error.error;
        this.errorList = error.error.error_list;
      })

    } else {
      this.error = true;
      this.errorMessage = 'Please fill all the fields!'
    }
  }

  companiesList() {
   return this.api.listCompanies().subscribe(companies => {
      companies.forEach(company => {
        this.companies.push({
          companyName: company.name,
          companyUrl: company.url
        });
      });
      this.sortArray(this.companies);
    });
  }

  groupsList() {
    return this.api.listGroups().subscribe(groups => {
      groups.forEach(group => {
        if(group.name != 'subscriber'){
          this.groups.push({
            groupName: group.name.replace('_',' '),
            groupUrl: group.url
          })  
        }
      })
    })
  }

  sortArray(array) {
    array.sort(function(a, b) {
      return a.companyName.toLowerCase().localeCompare(b.companyName.toLowerCase());
    });
  }

}
