import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Title } from '@angular/platform-browser';
import { ApiService } from '../../services/api.service';
import { DataService } from '../../services/data.service';
import { companiesEndPointUrl } from '../../credentials/credentials';

@Component({
  selector: 'app-add-domain',
  templateUrl: './add-domain.component.html',
  styleUrls: ['./add-domain.component.scss']
})
export class AddDomainComponent implements OnInit {

  domainValue: string = '';
  successMessage : string;
  errorMessage: string;
  companyURI: string;

  constructor(private location: Location, private api: ApiService,
    private data: DataService, private activatedRoute: ActivatedRoute, private title:Title) { }

  ngOnInit() {
    this.title.setTitle('Add domain - HamzaSite Discovery')
    let companyuuid = this.activatedRoute.snapshot.paramMap.get('companyId');
    this.companyURI = companiesEndPointUrl + companyuuid + '/';
  }

  goBack() {
    return this.location.back();
  }

  createDomain(domainValue) {
    if (domainValue.length > 1) {
      let domainInfo = {
        "name": domainValue,
        "company": this.companyURI
      }

      return this.api.createDomain(domainInfo).subscribe(result => {
        this.successMessage = "The domain name " + result.name + " was created successfully!";
        this.data.setMessage(this.successMessage);
        this.goBack();
      }, err => {
        console.log('err ', err.error)
        this.errorMessage = err.error.error;
      })
    } else {
      this.errorMessage = 'Please fill the field below';
    }
  }

}
