import { Component, OnInit, TemplateRef, ViewChild, ElementRef } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from '../../services/api.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

@Component({
  selector: 'app-company-connectors-settings',
  templateUrl: './company-connectors-settings.component.html',
  styleUrls: ['./company-connectors-settings.component.scss']
})
export class CompanyConnectorsSettingsComponent implements OnInit {

  connectors: Array<any>
  companyId: string;
  companyUrl: string;
  companyName: string;
  connectorType: string;
  connectorName: string;
  modalRef: BsModalRef;
  connectorsArray: Array<any>
  connectorUrl: any;
  connectorSettings: any;
  connectionParameters: any;
  errorTitle: string;
  errorDetail: string;
  errorList:any = [];
  loading: boolean = false;


  @ViewChild('enableConnectorErrorModal') enableConnectorErrorModal: TemplateRef<any>;

  constructor(private title: Title, private api: ApiService, private route: ActivatedRoute, private modalService: BsModalService) { }

  ngOnInit() {
    this.companyId = this.route.snapshot.paramMap.get('companyId');
    this.companyConnectors(this.companyId)
    this.companyConnectorDetails(this.companyId)
  }

  openModal(template: TemplateRef<any>) {
    this.errorTitle = '';
    this.modalRef = this.modalService.show(template, { class: 'share-link-modal' });
    this.modalService.onHide.subscribe(() => {
      this.errorTitle = ''
      this.errorDetail = ''
    })
  }


  companyConnectors(companyId) {
    this.loading = true;
    this.api.getCompanyInformation(companyId).subscribe(data => {
      this.companyName = data.name;
      this.title.setTitle(`${data.name} connectors - Sales - HamzaSite Discovery`);
      this.loading = false;
    })
  }

  selectedConnector(selected) {
    this.connectorUrl = selected.url;
    this.connectorName = selected.name;
  }

  addConnector(connectorName) {
    let connectorData = {
      "company": this.companyUrl,
      "connector": this.connectorUrl,
      "end_date": "2999-01-01",
      "name": connectorName
    }

    this.api.addConnector(connectorData).subscribe(data => {

      this.modalRef.hide();

      this.connectors.push({
        "companyUrl": data.company.url,
        "connectorUrl": data.connector.url,
        "status": data.status,
        "connectorType": data.connector.name,
        "connectorName": data.name,
        "uuid": data.uuid,
        "params": data.connection_parameters
      })

      this.errorTitle = ''
      this.errorDetail = ''

    }, error => {
      this.errorTitle = error.error.error;
      this.errorDetail = 'field:' + error.error.error_list[0].field + '  ' + error.error.error_list[0].errors;
      console.log('Error ', error)
    })
  }

  editConnectorInfo(connectorInfo: TemplateRef<any>, connector) {
    this.errorTitle = '';
    this.connectorSettings = connector;
    this.connectorName = connector.connectorName;
    this.connectionParameters = JSON.stringify(connector.params);
    this.modalRef = this.modalService.show(connectorInfo, { class: 'share-link-modal' });
    this.errorTitle = ''
    this.errorDetail = ''
  }

  checkJSONValidity(connectionParams) {
    try {
      JSON.parse(connectionParams);
    } catch (e) {
      this.errorTitle = "Error: Invalid JSON format'";
      return false;
    }
    return true;
  }

  saveSettings(connectionParameters, connectorName) {
    let connectionParams = connectionParameters.replace(/'/g, '"');

    this.checkJSONValidity(connectionParams)

    let info = {
      connector: this.connectorSettings.connectorUrl,
      company: this.connectorSettings.companyUrl,
      name: connectorName,
      connection_parameters: connectionParams
    }

    this.api.updateCompanyConnector(this.connectorSettings.uuid, info).subscribe(data => {

      this.connectors.forEach(company => {
        if (company.uuid === data.uuid) {
          company.params = data.connection_parameters;
          company.connectorName = data.name;
        }
      })

      this.errorTitle = ''
      this.errorDetail = ''

      this.modalRef.hide();
    }, error => {
      this.errorTitle = error.error.error_list[0].errors[0] || error.error.error;
      this.errorDetail = error.error.error_list[0].errors.message;
      console.log('Error ', error);
    })
  }

  deleteConnector(connectorInfo){
    this.api.deleteConnector(connectorInfo.uuid).subscribe(res=>{
      this.connectors.forEach((connector, i)=>{
        if(connector.uuid == connectorInfo.uuid){
          this.connectors.splice(i, 1)
        }
      })
    }, error=>{
      this.errorTitle = error.error_list[0].errors;
      console.log('error ', error);
    })
  }

  companyConnectorDetails(companyId) {
    this.api.enabledDisabledConnectors(companyId).subscribe(data => {

      var connectors = data[0]
      var companyConnectors = data[1]

      this.connectorsArray = connectors;

      this.companyUrl = companyConnectors.url;

      var connectorsArray = [];

      companyConnectors.connectors.forEach(companyConnector => {

        connectors.forEach(connector => {

          if (companyConnector.connector.uuid == connector.uuid) {
            connectorsArray.push({
              "companyUrl": companyConnector.company.url,
              "connectorUrl": connector.url,
              "status": companyConnector.status,
              "connectorType": connector.name,
              "connectorName": companyConnector.name,
              "uuid": companyConnector.uuid,
              "params": companyConnector.connection_parameters
            })
          }
        })

      })

      this.connectors = connectorsArray;
      this.sortArray(connectors);

    }, error => {
      console.log('err  >>>> ', error)
    })
  }

  sortArray(array) {
    array.sort(function(a, b) {
      return a.name.toLowerCase().localeCompare(b.name.toLowerCase());
   });
  }

  changeStatus(connector, event) {

    console.log('connector ', connector, event.currentTarget.checked)
    if (connector.status == 'active') {
      connector.status = 'inactive';

      let info = {
        connector: connector.connectorUrl,
        company: connector.companyUrl,
        status: connector.status
      }

      this.api.changeStatus(connector.uuid, info).subscribe(result => {
        console.log('result ', result)
      }, error => {
        console.log('err ', error)
      })

    }
    else if (connector.status == 'inactive') {
      // Check if connector type is SharePoint and has domain and realm.
      if (connector.connectorType == 'SharePoint' && connector.params == '{}') {
        event.currentTarget.checked = false;
        this.openModal(this.enableConnectorErrorModal);
        return;
      } else {
        connector.status = 'active';

        let info = {
          connector: connector.connectorUrl,
          company: connector.companyUrl,
          status: connector.status
        }

        this.api.changeStatus(connector.uuid, info).subscribe(result => {
          console.log('result ', result)
        }, error => {
          console.log('err ', error)
        });
      }
    }
  }
}
