import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyConnectorsSettingsComponent } from './company-connectors-settings.component';

describe('CompanyConnectorsSettingsComponent', () => {
  let component: CompanyConnectorsSettingsComponent;
  let fixture: ComponentFixture<CompanyConnectorsSettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompanyConnectorsSettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyConnectorsSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
