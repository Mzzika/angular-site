import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ApiService } from '../../services/api.service';
import { Router } from '@angular/router';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-create-client',
  templateUrl: './create-client.component.html',
  styleUrls: ['./create-client.component.scss']
})
export class CreateClientComponent implements OnInit {

  error: boolean = false;
  clientName: string = '';
  noOfLicenses: number;
  errorMessage: string;
  successMessage: string;
  errorList:any = [];

  constructor(private title: Title, private api: ApiService, private data: DataService, private route:Router) { }

  ngOnInit() {
    this.title.setTitle('Add company - Sales - HamzaSite Discovery');
  }

  createClient() {
    if (this.clientName && this.noOfLicenses) {
      let formData = {
        name: this.clientName,
        num_subscriptions: this.noOfLicenses
      }

      this.api.createCompany(formData).subscribe(data => {

        this.route.navigate(['admin/sales/companies'])
        this.data.setMessage('Company created successfully')
        this.error = false;

      }, error => {
        this.error = true;
        this.errorMessage = error.error.error;
        this.errorList = error.error.error_list;
      })
    } else {
      this.error = true;
      this.errorMessage = 'Please fill all the fields!'
    }
  }
}
