import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { ApiService } from '../../services/api.service';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-update-domain',
  templateUrl: './update-domain.component.html',
  styleUrls: ['./update-domain.component.scss']
})
export class UpdateDomainComponent implements OnInit {

  domainValue: string = '';
  errorMessage: string;
  companyURL: string;
  domainURL: string;

  constructor(private location: Location, private api: ApiService, private data: DataService,
    private title: Title, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.title.setTitle('Update domain - HamzaSite Discovery');
    this.companyURL = this.activatedRoute.snapshot.paramMap.get('companyURL');
    this.domainURL = this.activatedRoute.snapshot.paramMap.get('domainURL');
  }

  goBack() {
    this.location.back();
  }

  updateDomain(domainValue) {
    if (domainValue.length > 1) {

      let domainInfo = {
        "name": domainValue,
        "company": this.companyURL
      }

      return this.api.updateDomain(domainInfo, this.domainURL).subscribe(result => {
        let successMessage = "The domain name " + result.name + " has been updated successfully!";
        this.data.setMessage(successMessage);
        this.goBack();
      }, err => {
        console.log('err ', err.error)
        this.errorMessage = err.error.error;
      })
    } else {
      this.errorMessage = 'Please fill the field below';
    }
  }

}
