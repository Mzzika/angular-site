import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from '../../services/api.service';
import { Title, Meta } from '@angular/platform-browser';

@Component({
  selector: 'app-accept-invite',
  templateUrl: './accept-invite.component.html',
  styleUrls: ['./accept-invite.component.scss']
})

export class AcceptInviteComponent implements OnInit {

  validKey: boolean = false;
  error: boolean = false;
  errorMessage: string = '';
  fullName: string = ''
  password = ''
  email:string = '';
  passwordConfirmation = ''
  invitationKey: string;
  inviteType: string;
  successMessage: string;
  errorsList:any;

  constructor(private route: ActivatedRoute, private api: ApiService, private router: Router, private title: Title, private meta: Meta) { }

  ngOnInit() {
    this.meta.addTag({ name: 'robots', content: 'noindex' });
    this.title.setTitle('Create new account - HamzaSite Discovery');
    this.checkAuthencity()
    this.invitationKey = this.route.snapshot.paramMap.get('invitationKey');
    //Get invite type, public/private
    this.inviteType = this.route.snapshot.routeConfig.data.inviteType;
    if (this.inviteType == 'privateInvite') {
      this.checkPrivateInviteKey(this.invitationKey);
    } else {
      this.checkPublicInviteKey(this.invitationKey);
    }
  }

  checkAuthencity() {
    // check user login status and redirect if the user is logged in
    return this.api.checkTokenAuthenticity().subscribe(data => {
      this.router.navigate(['/admin/home'])
    })
  }

  checkPrivateInviteKey(invitationKey) {
    return this.api.checkPrivateInvitationKey(invitationKey).subscribe(res => {
      this.validKey = true;
    }, error => {
      this.errorMessage = error.error;
    })
  }

  checkPublicInviteKey(invitationKey) {
    return this.api.checkPublicInvitationKey(invitationKey).subscribe(res => {
      this.validKey = true;
    }, error => {
      this.errorMessage = error.error;
    })
  }

  createUser() {
    if (!this.fullName || !this.password || !this.password) {
      this.error = true;
      this.errorMessage = 'Please fill all fields';
      return;
    } else {
      if (this.password !== this.passwordConfirmation) {
        this.error = true;
        this.errorMessage = 'Passwords do not match!';
        return;
      }
      return this.api.createUserFromInvite(this.fullName, this.password, this.invitationKey, this.inviteType, this.email).subscribe(res => {
        this.successMessage = `Congratulations ${this.fullName} you have successfully registered with HamzaSite Discovery! Please check you inbox in order to complete the registration process`;
      }, error => {
        this.error = true;
        this.errorMessage = error.error.error;
        this.errorsList = error.error.error_list;
      })
    }
  }
}
