import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { ApiService } from '../../services/api.service';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-update-user',
  templateUrl: './update-user.component.html',
  styleUrls: ['./update-user.component.scss']
})
export class UpdateUserComponent implements OnInit {

  selectedRole = '';
  fullName: string = '';
  email: string = '';
  group: any = '';
  groups: any = [];
  company: any = '';
  error: boolean;
  errorMessage: string = ''
  successMessage: string = ''
  userCreatedSuccessfully = false;
  companies = [];
  userData: any;
  userId:string;
  errorList:any = [];
  companyUrl:string;
  groupUrl:string;


  constructor(private title: Title, private api: ApiService, private data: DataService, private route:Router) { }

  ngOnInit() {
    this.title.setTitle('Update user - Admin - HamzaSite Discovery');
    this.companiesList();
    this.groupsList();
    this.getUserData();
  }

  getUserData() {
    let userData = this.data.getAuthData();
    this.fullName = userData.first_name + ' ' + userData.last_name;
    this.email = userData.email;
    this.userId = userData.uuid;
    this.company = userData.company.name;
    this.companyUrl = userData.company.url;
    this.group = "< Select role >"
  }

  selectedGroup(group) {
    console.log('group ', group)
    this.groupUrl=group
  }

  selectedCompany(company) {
    console.log('company ', company)
    this.companyUrl = company;
  }

  updateUser(userId) {

    var formData = {
      "first_name": this.fullName,
      "email": this.email,
      "company": this.companyUrl,
      "groups": [this.groupUrl],
      "userId":this.userId
    }
    //console.log('formData ', formData)

    if (this.fullName && this.email) {

      this.api.updateUser(formData).subscribe(data => {

        this.route.navigate(['admin/super-admin/users'])
        this.data.setMessage('User updated successfully!')
        this.error = false;

      }, error => {
        this.error = true;
        this.errorMessage = error.error.error;
        this.errorList = error.error.error_list;
        //console.log('mess ', error.error_list)
      })

    } else {
      this.error = true;
      this.errorMessage = 'Please fill all the fields!'
    }
  }

  companiesList() {
    return this.api.listCompanies().subscribe(companies => {
      //console.log('companies ', companies)
      companies.forEach(company => {
        this.companies.push({
          companyName: company.name,
          companyUrl: company.url
        })
      })
    })
  }

  groupsList() {
    return this.api.listGroups().subscribe(groups => {
      groups.forEach(group => {
        if(group.name != 'subscriber'){
          this.groups.push({
            groupName: group.name.replace('_',' '),
            groupUrl: group.url
          })
        }
      })
    })
  }

}
