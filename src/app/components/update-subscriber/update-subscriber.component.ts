
import { groupsEndPointUrl } from './../../credentials/credentials';
import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';
import { ApiService } from '../../services/api.service';
import { Title } from '@angular/platform-browser';



@Component({
  selector: 'app-update-subscriber',
  templateUrl: './update-subscriber.component.html',
  styleUrls: ['./update-subscriber.component.scss']
})
export class UpdateSubscriberComponent implements OnInit {

  subscriberData: any;
  fullName: string;
  error: boolean = false;
  errorMessage: string;
  groups: any;
  successMessage: string;

  constructor(private title: Title, private data: DataService, private api: ApiService) { }

  ngOnInit() {
    this.subscriberData = this.data.getAuthData();
    this.fullName =  this.subscriberData.user.first_name +' ' + this.subscriberData.user.last_name;
    this.title.setTitle('Update subscriber '+ this.subscriberData.user.first_name +' ' + this.subscriberData.user.last_name+ '- HamzaSite Discovery');
  }

  updateSubscriber() {
    this.error = false;
    this.errorMessage = '';
    if (this.fullName == '' || this.fullName == undefined || this.fullName == ' ') {
      this.error = true;
      this.errorMessage = 'Subscriber name cannot be blank!';
      return;
    }
    let userInfo = {
      userId: this.subscriberData.user.uuid,
      first_name: this.fullName.split(' ')[0],
      last_name: this.fullName.split(' ')[1],
      email : this.subscriberData.user.email,
      company: this.subscriberData.company.url,
      groups: [groupsEndPointUrl+ '4/']
    }
    this.api.updateUser(userInfo).subscribe(data => {
      this.successMessage = 'Update successfully done!'
    }, 
    (err) => {
      this.error = true;
      this.errorMessage = err.error.error;
      console.log(err)
      }
    )
  }

}
