import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from '../../services/api.service';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-role',
  templateUrl: './role.component.html',
  styleUrls: ['./role.component.scss']
})
export class RoleComponent implements OnInit {

  userType = '';
  error = false;
  errorMessage = '';
  userEmail:string;
  userGroup:string;

  constructor(private title: Title, private route: ActivatedRoute,
    private router: Router, private api: ApiService, private data:DataService) { }

  ngOnInit() {
    this.data.setMessage(null);
    this.title.setTitle('Admin homepage - HamzaSite Discovery');
    this.checkAuthencity()
    // Check url and set appropriate url to avoid 404 error.
    let url = this.router.url;
    if (url == '/') {
      this.router.navigate(["admin/home"], {replaceUrl:true});
    }
  }

  checkAuthencity() {
    return this.api.checkTokenAuthenticity().subscribe(data => {
      this.userEmail = data.email;
      this.userGroup = data.access_level.replace('_',' ');
      data.groups.forEach(group => {
        this.userType = group
      })

    }, error => {
      this.error = true;
      this.errorMessage = error.error;
      this.router.navigate(['/admin/login'])
    })
  }

  logOut() {
    this.api.logOut().subscribe(result=>{}, error=>{console.log('err ', error)})
    this.router.navigate(["/admin/login"]);
  }

}
