
Release notes - HamzaSite Discovery - Version Hydra_1.0.0
---------------------------------------------------------

* Changed pipelines for pro deployment
* To Production on 07/06/2018
* Initial release
* Fix Pro deployment issues


Release notes - HamzaSite Discovery - Version HYDRA_v1.5.0
------------------------------------------------------

### Sub-task
* [DIS-283] - Add resend invitation link in user list in order to resend invitations
* [DIS-381] - Add new Icon into Hydra
* [DIS-388] - Compile all strings to translate from Hydra
* [DIS-395] - Create designs for each new / update screen
* [DIS-396] - Apply new designs into frontend and also change some of the URL
* [DIS-408] - Add icon into Hydra all results page

### Bug
 * [DIS-375] - Number of free subscriptors is not correct
 * [DIS-385] - TypeError: this.api.search(...) is undefined
 * [DIS-386] - TypeError: Cannot read property 'inviteType' of undefined
 * [DIS-391] - TypeError: Cannot read property 'subscribe' of undefined
 * [DIS-410] - Error: If ngModel is used within a form tag, either the name attribute must be set or the form
 * [DIS-411] - Build pro does not work in Hydra
 * [DIS-413] - Error: Non-Error exception captured with keys: error, status_code
 * [DIS-415] - I don't see error messages in the login page
 * [DIS-418] - In the user admin appears the company invite link.
 * [DIS-422] - We shall not use any CDN
 * [DIS-425] - The "resend invitation" action remains although invitation was accepted
 * [DIS-427] - Building hydra appears an error about searchLimit not in SearchComponent.
 * [DIS-430] - Results not displayed initially
 * [DIS-433] - Public invitation link screen fails as Invalid Token (when logged out)
 * [DIS-442] - All results page issues with Office365 connector
 * [DIS-456] - Company add connector: Is not working. 
 * [DIS-458] - Company connectors: Active / inactive not right

### Story
 * [DIS-167] - As HamzaSite client who subscribes to Discovery I want to be able to specify in the Admin backend which users can use Discovery 
 * [DIS-245] - As a user I want to see more than one connector results in my google search and all results page so I can see all my content in one place
 * [DIS-367] - As a developer I want to see enhancements in the frontend

### New Feature
* [DIS-417] - As a user I don't want to see connect to forrester when forrester does not have login

### Improvement
* [DIS-374] - Change the frontend where it says client change to Company text
* [DIS-387] - Translations - get strings
* [DIS-424] - Sort must be improved in the all result page.


Release notes - HamzaSite Discovery - Version HYDRA_v2.0.0
------------------------------------------------------

### Sub-task
* [DIS-468] - Frontend: Add new field in connector parameters to update / add soft-name to the connector 
* [DIS-472] - Use icon in Hydra
* [DIS-476] - Apply thumbnail switch in Hydra

### Bug
* [DIS-428] - You are not logged in message. 
* [DIS-431] - Errors in create user screen
* [DIS-434] - Error description not shown in hydra when creating new subscriber via public invitation
* [DIS-448] - I am logged-in in the extension, but when I try to access hydra I am asked to login again
* [DIS-449] - Some of the switchers looks without borders
* [DIS-450] - Users list: disable / enable does not work
* [DIS-451] - User update: When updating, the button says create instead of update
* [DIS-452] - User update: When I click create (update) validation errors appear that should not appear. 
* [DIS-453] - Users add: If a super admin adds a new user, the email message appear and should not appear. Redirect to users list and success message should appear there
* [DIS-454] - Create company: When created, you don't return to companylist
* [DIS-455] - Update company: When update a client, I see a screen with success message instead of being redirected to company list
* [DIS-457] - Company connector: Back home should be back
* [DIS-459] - Update company connector properties: fail!
* [DIS-460] - Comapny connectors: Delete does not work
* [DIS-461] - Subscriers: When I put my mouse on the invitation link it does not change the cursor,
* [DIS-462] - Error when disconnect from other tab
* [DIS-463] - Subscribers list: if I deactivate someone, automatically should be marked as grey
* [DIS-465] - Change authentification from Token to Cookie
* [DIS-485] - vendor.bundle.js is too big it kills the client
* [DIS-486] - Company add connector: Is not working.
* [DIS-487] - Company connectors details: When I change the properties, I have to reload to see the change
* [DIS-488] - TypeError: this.api.getCompanyInformation(...) is undefined
* [DIS-489] - TypeError: Cannot read property 'subscribe' of undefined
* [DIS-490] - TypeError: Cannot read property 'replace' of undefined
* [DIS-491] - TypeError: Cannot read property 'replace' of undefined
* [DIS-492] - Error: Non-Error exception captured with keys: error, status_code
* [DIS-493] - Error: If ngModel is used within a form tag, either the name attribute must be set or the form
* [DIS-495] - TypeError: _co.companyData is undefined
* [DIS-496] - User list: If I go to edit, and go back button, I see the following
* [DIS-499] - TypeError: Cannot read property 'name' of undefined
* [DIS-500] - Error: If ngModel is used within a form tag, either the name attribute must be set or the form
* [DIS-501] - TypeError: Cannot read property 'replace' of undefined
* [DIS-517] - No results returned in all result page

### Story
* [DIS-34] - As an employee I want to be able to switch between list and thumbnail view of DISCOVERY results
* [DIS-360] - Connector: HamzaSite Collections
* [DIS-394] - As a sales admin I want to be able to add more than one connector of the same type to a company so I can assign multiple instances of one connector

### Improvement
* [DIS-432] - subscriber/login is no longer needed. Can be removed.	


Release notes - HamzaSite Discovery - Version HYDRA_v2.5.0
------------------------------------------------------

### Bug
* [DIS-437] - After register with public invite the user is instructed to go login page instead of activation email
* [DIS-509] - add new company conenctor breaks everything

### New Feature
* [DIS-494] - Design 404 and 500 pages for Discovery

### Improvement
* [DIS-554] - All result page: Change Text 


Release notes - HamzaSite Discovery - Version HYDRA_v2.6.0
------------------------------------------------------

### Bug
* [DIS-508] - Editing company_connector show validation failed... but I haven't changed anything yet
* [DIS-519] - All result page: apply style to select connector
* [DIS-523] - query to auth/me when login page (not logged in).
* [DIS-560] - I'm always redirected to https://discoveryalpha.HamzaSite.net/app#/404

### Improvement
* [DIS-550] - Apply 404 and 500 pages to hydra
* [DIS-553] - Overview Page needs Search Field


Release notes - HamzaSite Discovery - Version HYDRA_v3.0.0
------------------------------------------------------

### Bug
* [DIS-525] - All result page: If the result does not have thumbnail. The icon should be shown
* [DIS-548] - Discovery: No sufficient error handling on 'change password' dialogue
* [DIS-568] - If I navegate to https://discoveryalpha.HamzaSite.net/app/ I got a 404 error
* [DIS-569] - if I'm in manage my connectors and click back i get 404 error
* [DIS-570] - 404 page buttons does not work
* [DIS-574] - ERROR in src/app/components/client-admin-home/client-admin-home.component.html(4,10): : Property 'cancel' does not exist on type 'ClientAdminHomeComponent'.
* [DIS-575] - When I try to login with incorrect password I get [object object] error
* [DIS-576] - From user list I click back and I got 404 error
* [DIS-622] - Hydra: Reset password - Enter incorrect email
* [DIS-623] - Hydra: When password has changed, change the button label from log-in to log in
* [DIS-624] - Hydra: Users - Create new - Companies are not sorted
* [DIS-625] - Hydra: Users - Create new - Error validating groups and company
* [DIS-629] - Hydra: "go home" button navigates to previous page instead of admin home
* [DIS-631] - Hydra: Edit an existing user: Roles dropdown has incorrect values
* [DIS-633] - Hydra - Update an user is throwing an error
* [DIS-634] - Hydra: Companies list - Back button does not return to home
* [DIS-635] - Hydra: Create a company screen - Enter letters in number of licenses
* [DIS-636] - Hydra: Companies - Company connectors - Add connector - Connector list is not sorted
* [DIS-638] - If I change quickly between pages, I get 404 error page
* [DIS-639] - Hydra: add connector leaving the name blank
* [DIS-641] - Hydra: Enable a connector that does not have the minimum connector parameters
* [DIS-642] - Hydra: Editing an existing company connector parameters
* [DIS-647] - Pluto: Updating connection parameters to office365 connector causes exception in backend

### New Feature
* [DIS-559] - In manage my connectors, I should be able to enable / disable them

### Improvement
* [DIS-630] - Add loading icon while the Hydra data pages are being filled


Release notes - HamzaSite Discovery - Version HYDRA_v3.1.0
------------------------------------------------------

### Sub-task
* [DIS-370] - Set up docker and Pipelines
* [DIS-380] - Test the build and tests

### Bug
* [DIS-539] - Error: Non-Error exception captured with keys: error, headers, message, name, ok…
* [DIS-541] - TypeError: Cannot read property 'length' of undefined
* [DIS-542] - TypeError: Cannot read property 'subscribe' of undefined
* [DIS-546] - TypeError: Cannot read property 'length' of undefined
* [DIS-637] - Navigate in Microsoft Edge does not work in HYDRA
* [DIS-643] - Hydra: List pages should be sorted by name column. Maybe use a javascript to manage tables
* [DIS-645] - In subscribers list. I click on the HamzaSite logo and I'm redirected to 404 page
* [DIS-646] - Update subscriber - Page title does not change
* [DIS-651] - Hydra: Search on "all results" page displays false results
* [DIS-652] - hydra: All results page drop down shows all connectors instead of only the ones with results like in the extension
* [DIS-655] - Change connectors of a company with bad json shows a weird error
* [DIS-657] - Update subscriber data does not work at all
* [DIS-658] - Invite subscribers page title is not right
* [DIS-659] - Accept invitation, passwords does not match - Error is not correct
* [DIS-660] - Password is not checked for complexity
* [DIS-661] - Invite user from link does not work at all
* [DIS-670] - Pluto: Connector returns error in web socket response when searching multiple times in all results page.
* [DIS-676] - TypeError: n.companyData is undefined
* [DIS-677] - TypeError: n.connectors is undefined
* [DIS-680] - Hydra: for reset password link a missing language file results in 404 page 
* [DIS-681] - Pluto: API /auth/password/reset does not check if user with this email exist 
* [DIS-683] - Build error in hydra: UpdateSubscriberComponent
* [DIS-685] - ie11 I have to login everytime I open the browser. 
* [DIS-686] - [object object] in connector_properties field
* [DIS-689] - manage my connector does not filter by status

### Story
* [DIS-354] - As a developer I want frontend test with Jasmine & Karma to be implemented so I have much control of the errors

### Improvement
* [DIS-673] - Discovery Extension - Login 
* [DIS-691] - Add hydra development mode to orchestrator.sh start
* [DIS-692] - Add translations








