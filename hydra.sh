#!/usr/bin/env bash

set -e
DOCKER_APP_IMAGE_NAME=powll/hydra:latest
export COMPOSE_FILE=docker/docker-compose.yml
export COMPOSE_PROJECT_NAME=pluto
COMMAND=${1}

if [ -z ${COMMAND} ] ; then
    echo 'Missing argument'
    echo '   ./hydra.sh build                      Build docker image'
    echo '   ./hydra.sh build_static        	   Build development static files and environment files'
    echo '   ./hydra.sh build_static_production    Build production static files and environment files'
    echo '   ./hydra.sh build_static_beta          Build beta static files and environment files'
    echo '   ./hydra.sh build_static_alpha         Build alpha static files and environment files'
    echo '   ./hydra.sh serve                      Run angular development server using docker-compose'
    echo '   ./hydra.sh test                       Run angular test'
    echo '   ./hydra.sh stop                       Stop angular development server using docker-compose'
    exit
fi

# Build dev environment
if [ ${COMMAND} = 'build' ] ; then
    docker build -f docker/Dockerfile --pull -t ${DOCKER_APP_IMAGE_NAME} .

# Allow multiple commands to support intuitive usage
elif [ ${COMMAND} = 'start' -o ${COMMAND} = 'run' ] ; then
    # Run the dev server
    docker-compose up

elif [ ${COMMAND} = 'build_static' ] ; then
    docker run --rm ${DOCKER_APP_IMAGE_NAME} build_static

elif [ ${COMMAND} = 'build_static_production' ] ; then
    docker run --rm ${DOCKER_APP_IMAGE_NAME} build_static_production

elif [ ${COMMAND} = 'build_static_beta' ] ; then
    docker run --rm ${DOCKER_APP_IMAGE_NAME} build_static_beta

elif [ ${COMMAND} = 'build_static_alpha' ] ; then
    docker run --rm ${DOCKER_APP_IMAGE_NAME} build_static_alpha

elif [ ${COMMAND} = 'test' ] ; then
    docker run --rm ${DOCKER_APP_IMAGE_NAME} test

elif [ ${COMMAND} = 'serve' ] ; then
    docker-compose up -d

elif [ ${COMMAND} = 'stop' -o ${COMMAND} = 'down' -o ${COMMAND} = 'halt' ] ; then
    # Remove dev container(s)
    docker-compose down
fi
